﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace hello.nett.Models
{
    public class Mahasiswa
    {
        [Required(ErrorMessage = "Nama tidak boleh kosong")]
        public string Nama { get; set; }
        [Required(ErrorMessage = "NIM tidak boleh kosong")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "NIM harus berbentuk angka")]
        public string NIM { get; set; }
        [Required(ErrorMessage = "Angkatan tidak boleh kosong")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Angkatan harus berbentuk angka")]
        public int Angkatan { get; set; }
        [Required(ErrorMessage = "Fakultas tidak boleh kosong")]
        public string Fakultas { get; set; }
    }
}