﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;

namespace hello.nett.Controllers
{
    public class DataTypeController : Controller
    {
        // GET: DataType
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Array()
        {
            string[] arr = new string[5];
            arr[0] = "Ini adalah teks pertama";
            arr[1] = "Ini adalah teks kedua";
            arr[2] = "Ini adalah teks ketiga";
            arr[3] = "Ini adalah teks keempat";
            arr[4] = "Ini adalah teks kelima";
            ViewBag.Array = arr;
            return View();
        }

        public ActionResult Hashtable()
        {
            Hashtable hashtab = new Hashtable();
            hashtab.Add(1, "Data Pertama");
            hashtab.Add("Dua", 2);
            hashtab.Add('3', true);
            hashtab.Add(false, 'A');
            ViewBag.HashTab = hashtab;
            return View();
        }

        public ActionResult Dictionary()
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            dictionary.Add("Pertama", 90);
            dictionary.Add("Kedua", 20);
            dictionary.Add("Ketiga", 37);
            dictionary.Add("Keempat", 49);
            ViewBag.Dict = dictionary;
            return View();
        }

        public ActionResult List()
        {
            List<string> list = new List<string>();
            list.Add("ini list pertama");
            list.Add("ini list kedua");
            list.Add("ini list ketiga");
            list.Add("ini list keempat");
            ViewBag.List = list;
            return View();
        }
    }
}