﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hello.nett.Models;

namespace hello.nett.Controllers
{
    public class MahasiswaController : Controller
    {
        // GET: Mahasiswa
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Mahasiswa mahasiswa)
        {
            if (!ModelState.IsValid)
                return View();
            return View("index", mahasiswa);
        }
    }
}